import './App.css';

function App() {
  return (
    
    <div className='wrapper'>
   
      <navbar className='container-fluid navbar shadow'>
      <h2>Wishbone+Partner</h2>

      <ul className='nav_ul'>
        <li><a className='a_nav' href='#'>Project</a></li>
        <li><a className='a_nav' href='#'>About</a></li>
        <li><a className='a_nav' href='#'>News</a></li>
        <li><a className='a_nav' href='#'>Team</a></li>
        <li><a className='a_nav' href='#'>Contact</a></li>
      </ul>
     <button className="btn_nav">Get Tempate</button>
      </navbar>

      <main>
        <section className='section-1'>
        <div className='section-1_text'>
          <div className='top_text'>
            <p>Wishbone+Partner</p>
            <h1>The home of beautiful architectura.</h1>
          </div>

          <div className='bottom_text'>
            <p>We are an architectura firm with a focus on beatiful but functional design.At its heart,we believe design is about usability an accessibility-these are the guiding principles for our work.Read more about our previous projects, our process and our team below</p>
            <p className='readMore'><a href='#'>Read more</a></p>
          </div>

        </div>



        <div className='section-2_img'>
          <img  src='./image/IMAGE.svg'/>
        </div>

        
          </section>



          <section className='section-2'>
            <div className='section-2_box-1'><img className='section_img' src='./image/Our firm.svg' /></div>
            <div className='section-2_box-2'>
               <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Porro voluptates soluta ducimus distinctio necessitatibus aut cupiditate. Facilis optio quos consequatur in facere. Mollitia soluta odit accusamus cupiditate minima ut quidem.</p>
               <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Porro voluptates soluta ducimus distinctio necessitatibus aut cupiditate. Facilis optio quos consequatur in facere. Mollitia soluta odit accusamus cupiditate minima ut quidem.</p>
               <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Porro voluptates soluta ducimus distinctio necessitatibus aut cupiditate. Facilis optio quos consequatur in facere. Mollitia soluta odit accusamus cupiditate minima ut quidem.</p>
               <img className='section-2_box-2-img' src='./image/Frame.svg' />
        </div>

          </section>


          <section className='section-3'>
          <img className='section-3Img' src='./image/3-.svg' />
          </section>



          <section className='section-4'>

            <img className='section-4Img' src="./image/section-4.svg" />

          </section>

          <section className='section-5'>
            <img className='section-5Img' src='./image/section-5.jpg' />
          </section>


          
          <section className='section-6'>
            <img className='section-6Img' src='./image/section-6-org.svg' />
          </section>


          <section className='section-7'>
            <img className='section-7Img' src='./image/section7org.svg' />
          </section>

          <section className='section-8'>
            <img className='section-8Img' src='./image/section-8.svg' />
          </section>

          <section className='section-9'>
              <div className='section-9Box1'>
          <img className='section-9Img' src='./image/section-9 (2).svg' />
              </div>
              <div className='section-9Box2'>
                <img className='section-9Img2' src='./image/section-9Img2.svg' />
              </div>

          </section>


          <section className='section-10'>
            <img className='section-10Img' src='./image/section-10.svg' />
          </section>

      </main>

      <footer>
          <img className='footerImg' src='./image/footer.svg' />
      </footer>


     
  </div>
  );
}

export default App;
